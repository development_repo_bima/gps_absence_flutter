import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

/*
 * class Storage
 * handle storage case: set, get, contains
 * create by <Bima Samudra> <bimarossoneri@gmail.com>
*/
class Storage {
  static SharedPreferences localStorage;
  List<String> result;

  static Future init() async {
    localStorage = await SharedPreferences.getInstance();
  }

  static Future<bool> containStorage(key) async {
    await init();
    var status = await localStorage.containsKey(key);
    return status;
  }

  static Future setStringlist(String key, List value) async {
    await init();
    localStorage.setStringList(key, value);
  }

  static Future<List> getStringlist(String key) async {
    await init();
    return localStorage.getStringList(key);
  }

  static Future setStorage(List<String> key, List<String> value) async {
    await init();

    if (key.length > 1) {
      for (var i = 0; i < key.length; i++) {
        localStorage.setString(key[i], value[i]);
      }
    } else {
      localStorage.setString(key[0], value[0]);
    }
  }

  static Future<Map> getStorage(List<String> key) async {
    await init();
    var map = new Map<String, dynamic>();
    if (key.length > 1) {
      for (var i = 0; i < key.length; i++) {
        map[key[i]] = localStorage.getString(key[i]) ?? '';
        // this.result[key[i]] = localStorage.getString(key[i]) ?? '' ;
      }
    } else {
      map[key[0]] = localStorage.getString(key[0]) ?? '';
    }

    return map;
  }

  static Future clearStorage() async {
    await init();
    await localStorage.clear();
  }

  static Future removeStorage(List<String> key) async {
    await init();

    if (key.length > 1) {
      for (var i = 0; i < key.length; i++) {
        await localStorage.remove(key[i]);
      }
    } else {
      await localStorage.remove(key[0]);
    }
  }
}
