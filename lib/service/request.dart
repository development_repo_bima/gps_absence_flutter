import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:tps/service/storage.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

/* 
 * class Request
 * handle http request for whole apps
 * create by <Bima Samudra> <bimarossonerigmail.com>
 * GET and POST verb only !
 * can implement PUT, PATCH, DELETE verb request next
*/
class Request {
  Future<Map> postRequest(String url, {Map body}) async {
    List<String> keys = ['uuid'];
    Map storageData = await Storage.getStorage(keys);
    Map<String, String> header = {
      'accept': 'application/json; charset=utf-8',
      'Authorization': 'Bearer ' + storageData['uuid']
    };

    try {
      // if (storageData['token'].isNotEmpty) {
      return http
          .post(url, headers: header, body: body)
          .then((http.Response response) {
        final int statusCode = response.statusCode;
        // Toast.show(response.toString(), context,
        //   duration: 8, gravity: Toast.CENTER);
        // if (statusCode < 200 || statusCode > 400 || json == null) {
        //   throw new Exception("Error while fetching data");
        // }
        // print(response.toString());
        var msg;
        if (statusCode == 500) {
          msg = 'Terjadi Kesalahan Server, Hubungi administrator';
        } else if (statusCode == 404) {
          msg = 'Terjadi kesalahan, server tidak ditemukan';
        } else if (statusCode == 200) {
          return json.decode(response.body);
        }
        return {'status': false, 'msg': msg};
      });
      // } else {
      //   return {'status': false, 'msg': 'cannot fetch token from apps'};
      // }
    } on TimeoutException catch (_) {
      return {'status': false, 'msg': 'Timeout Reached'};
    } on SocketException catch (_) {
      return {'status': false, 'msg': 'Socket errors'};
    }
  }

  Future<Map> getRequest(String url) async {
    List<String> keys = ['uuid'];

    Map storageData = await Storage.getStorage(keys);

    try {
      // if (storageData['uuid'].isNotEmpty) {
      // MEMINTA DATA KE SERVER DENGAN KETENTUAN YANG DI ACCEPT ADALAH JSON
      Map<String, String> header = {
        'accept': 'application/json',
        'Authorization': 'Bearer ' + storageData['uuid']
      };
      var res = await http.get(Uri.encodeFull(url), headers: header);
      final int statusCode = res.statusCode;
      var msg;
      // if (statusCode < 200 || statusCode > 400 || json == null) {
      //   print(statusCode);
      // }

      if (statusCode == 500) {
        msg = 'Terjadi Kesalahan Server, Hubungi administrator';
      } else if (statusCode == 404) {
        msg = 'Terjadi kesalahan, server tidak ditemukan';
      } else if (statusCode == 200) {
        return json.decode(res.body);
      }
      return {'status': false, 'msg': msg};
      // } else {
      //   return {'status': false, 'msg': 'cannot fetch token from apps'};
      // }
    } on TimeoutException catch (_) {
      return {'status': false, 'msg': 'Timeout Reached'};
    } on SocketException catch (_) {
      return {'status': false, 'msg': 'Socket errors'};
    }
  }
}
