class Routing {
  static String update = 'https://api.bimsalabimm.space/api/update-kehadiran';
  static String post = 'https://api.bimsalabimm.space/api/post-data';
  static String login = 'https://api.bimsalabimm.space/api/login-data';
  static String postcuti = 'https://api.bimsalabimm.space/api/post-cuti';
  static String post_marketing =
      'https://api.bimsalabimm.space/api/post-data-marketing';
  static String report_marketing =
      'https://api.bimsalabimm.space/api/post-report-marketing';
  static String jumlah_cuti = 'https://api.bimsalabimm.space/api/count-cuti';

  static String outlet = 'https://api.bimsalabimm.space/api/get-outlet';
  static String product = 'https://api.bimsalabimm.space/api/get-product';
  static String karyawan = 'https://api.bimsalabimm.space/api/get-karyawan';
  static String inbox = 'https://api.bimsalabimm.space/api/get-inbox';
  static String absensi = 'https://api.bimsalabimm.space/api/get-absensi';
  static String historycuti = 'https://api.bimsalabimm.space/api/get-cuti';
  static String detail_kunjungan =
      'https://api.bimsalabimm.space/api/detail-kunjungan';
  static String inbox_marketing =
      'https://api.bimsalabimm.space/api/get-inbox-marketing';
  static String inboxManager =
      'https://api.bimsalabimm.space/api/get-inbox-manager';
  static String detail_inbox =
      'https://api.bimsalabimm.space/api/get-detail-inbox';
  static String reportMarketing =
      'https://api.bimsalabimm.space/api/get-report-kunjungan';
}
