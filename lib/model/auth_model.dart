class Post {
  final String nama;
  final String nip;
  final String uuid;
  bool status;
  final String role;
  final String roleId;
  final String msg;
  final String email;
  final String kantor;

  Post(
      {this.nama,
      this.nip,
      this.uuid,
      this.role,
      this.roleId,
      this.email,
      this.kantor,
      this.msg,
      this.status});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      uuid: json['uuid'],
      nama: json['nama'],
      email: json['email'],
      msg: json['msg'],
      status: json['status'],
      role: json['role'],
      nip: json['nip'],
      roleId: json['roleId'],
      kantor: json['kantor'],
    );
  }

  // Map toMap() {
  //   var map = new Map<String, dynamic>();
  //   map["username"] = username;
  //   map["password"] = password;

  //   return map;
  // }
}
