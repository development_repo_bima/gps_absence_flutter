import 'package:flutter/material.dart';

class Notifikasi extends StatefulWidget {
  Notifikasi({Key key}) : super(key: key);

  @override
  _NotifikasiState createState() => _NotifikasiState();
}

class _NotifikasiState extends State<Notifikasi> {
  var absensi = [
    {
      "title": "Pengajuan Perbaikan Absen",
      "keterangan": "Input perbaikan Absensi",
      "User": "Bima Samudra I0001",
      "dPosted": "2020-01-12"
    },
    {
      "title": "Pengajuan Perbaikan Absen",
      "keterangan": "Input perbaikan Absensi",
      "User": "Bima Samudra I0001",
      "dPosted": "2020-01-12"
    },
    {
      "title": "Pengajuan Perbaikan Absen",
      "keterangan": "Input perbaikan Absensi",
      "User": "Bima Samudra I0001",
      "dPosted": "2020-01-12"
    },
    {
      "title": "Pengajuan Perbaikan Absen",
      "keterangan": "Input perbaikan Absensi",
      "User": "Bima Samudra I0001",
      "dPosted": "2020-01-12"
    },
    {
      "title": "Pengajuan Perbaikan Absen",
      "keterangan": "Input perbaikan Absensi",
      "User": "Bima Samudra I0001",
      "dPosted": "2020-01-12"
    },
    {
      "title": "Pengajuan Perbaikan Absen",
      "keterangan": "Input perbaikan Absensi",
      "User": "Bima Samudra I0001",
      "dPosted": "2020-01-12"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Notifikasi"),
          actions: [],
        ),
        body: Container(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return Card(
                elevation: 2,
                borderOnForeground: true,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // Icon(Icons.android, size: 25),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              (absensi[index]["title"]) ?? '-',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              (absensi[index]["keterangan"]) ?? '-',
                              style: TextStyle(
                                  fontSize: 11, fontStyle: FontStyle.italic),
                            ),
                            Text(
                              (absensi[index]["User"]) ?? '-',
                              style:
                                  TextStyle(fontSize: 11, color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        (absensi[index]["dPosted"]) ?? '-',
                        style: TextStyle(fontSize: 10, color: Colors.blueGrey),
                      )
                    ],
                  ),
                ),
              );
            },
            itemCount: absensi.length,
          ),
        ));
  }
}
