import 'package:tps/activity/absensi_karyawan.dart';
import 'package:tps/activity/absensi_marketing.dart';
import 'package:tps/activity/cuti.dart';
import 'package:tps/activity/kehadiran.dart';
// import 'package:absensi_dev/activity/kunjungan.dart';
// import 'package:absensi_dev/activity/notifikasi.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/fragment/account.dart';
import 'package:tps/fragment/inbox.dart';
// import 'package:absensi_dev/fragment/profile.dart';
import 'package:tps/fragment/report.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:toast/toast.dart';
import 'package:trust_location/trust_location.dart';
// import 'package:toast/toast.dart';
import 'package:location_permissions/location_permissions.dart';
// import 'package:gpsm/component/widget_text.dart';

class Home extends StatefulWidget {
  final Function() onPressed;
  final String tooltip;
  final IconData icon;

  Home({this.onPressed, this.tooltip, this.icon});

  @override
  _HomeState createState() => _HomeState();
  // State<StatefulWidget> createState() {
  //   return _HomeState();
  // }
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  String _address, address_code = '-';
  bool mockStatus = false;
  bool loadLocation = false;
  final List<Widget> _children = [Inbox(), Report(), Account()];
  final List<String> _title = ["Pesan Masuk", "Absensi", "Akun"];

  // untuk FAB
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Position _currentPosition;

  String _latitude;
  String _longitude;
  Map data;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  /// get location method, use a try/catch PlatformException.
  Future<void> getLocation() async {
    try {
      TrustLocation.onChange.listen((values) => setState(() {
            _latitude = values.latitude;
            _longitude = values.longitude;
            mockStatus = values.isMockLocation;
          }));
    } on PlatformException catch (e) {
      print('PlatformException $e');
    }
  }

  /// request location permission at runtime.
  void requestLocationPermission() async {
    PermissionStatus permission =
        await LocationPermissions().requestPermissions();
    print('permissions: $permission');
  }

  _getCurrentLocation() {
    setState(() {
      loadLocation = true;
    });
    // requestLocationPermission();

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
      TrustLocation.start(3);
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      final coordinates =
          Coordinates(_currentPosition.latitude, _currentPosition.longitude);

      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);

      var obj = addresses.first;

      setState(() {
        _address = obj.addressLine + ' ' + obj.locality;
        loadLocation = false;
        // print(loadLocation);
      });
    } catch (e) {
      print(e);
    }
  }

  Future init() async {
    Map storage = await Storage.getStorage(['uuid', 'role_id']);
    setState(() {
      this.data = storage;
    });
    // Toast.show(data['role_id'], context, duration: 3, gravity: Toast.CENTER);
  }

  void initState() {
    init();
    _getCurrentLocation();
    getLocation();

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {});
          });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));

    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget absensi_marketing() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Card(
            child: (isOpened)
                ? Padding(
                    padding: EdgeInsets.all(8),
                    child: Fonts.subHeadings(
                        "Absensi Kunjungan", false, false, Colors.black),
                  )
                : Container(),
          ),
          FloatingActionButton(
            heroTag: "mrkt",
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Checkin()));
            },
            tooltip: 'Marketing',
            child: Icon(Icons.add_location_sharp),
          )
        ],
      ),
    );
  }

  Widget absensi_karyawan() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Card(
            child: (isOpened)
                ? Padding(
                    padding: EdgeInsets.all(8),
                    child: Fonts.subHeadings(
                        "Absensi Karyawan", false, false, Colors.black),
                  )
                : Container(),
          ),
          FloatingActionButton(
            heroTag: "absk",
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Absensi()));
            },
            tooltip: 'Add',
            child: Icon(Icons.add),
          )
        ],
      ),
    );
  }

  Widget cuti() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Card(
            child: (isOpened)
                ? Padding(
                    padding: EdgeInsets.all(8),
                    child: Fonts.subHeadings(
                        "Cuti Karyawan", false, false, Colors.black),
                  )
                : Container(),
          ),
          FloatingActionButton(
            onPressed: () {
              // Util.routing(context, Absensi(), true);

              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Cuti()));
            },
            heroTag: "cuti",
            tooltip: 'Cuti',
            child: Icon(Icons.airplanemode_active),
          ),
        ],
      ),
    );
  }

  Widget kehadiran() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Card(
            child: (isOpened)
                ? Padding(
                    padding: EdgeInsets.all(8),
                    child: Fonts.subHeadings(
                        "Update Kehadiran", false, false, Colors.black),
                  )
                : Container(),
          ),
          FloatingActionButton(
            onPressed: () {
              // Util.routing(context, Absensi(), true)
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Kehadiran()));
            },
            heroTag: "khdr",
            tooltip: 'Update Kehadiran',
            child: Icon(Icons.note_add),
          ),
        ],
      ),
    );
  }

  Widget toggle() {
    return FloatingActionButton(
      backgroundColor: _buttonColor.value,
      onPressed: animate,
      tooltip: 'Toggle',
      child: AnimatedIcon(
        icon: AnimatedIcons.menu_close,
        progress: _animateIcon,
      ),
      heroTag: "togl",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this._title[_currentIndex]),
        // actions: [
        //   IconButton(
        //     icon: Icon(Icons.notification_important),
        //     onPressed: () {
        //       // Util.routing(context, NotificationList(), true);
        //       Navigator.push(context,
        //           MaterialPageRoute(builder: (context) => Notifikasi()));
        //     },
        //     tooltip: "Notifikasi",
        //   )
        // ],
      ),
      body: Column(
        children: [
          Card(
              elevation: 2,
              child: Padding(
                padding: EdgeInsets.all(4),
                child: Row(
                  children: [
                    // Icon(Icons.location_on),
                    IconButton(
                        icon: Icon(Icons.location_on),
                        onPressed: () {
                          requestLocationPermission();
                          _getCurrentLocation();
                          getLocation();
                        }),
                    (loadLocation)
                        ? CircularProgressIndicator()
                        : Expanded(
                            child: Padding(
                            padding: EdgeInsets.all(2),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  (this._address) ?? '-',
                                  style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                                Text(
                                  "Debug Version ~ Mock Location :" +
                                      mockStatus.toString(),
                                  style: TextStyle(fontSize: 10),
                                ),

                                // WidgetText.subHeader("Your Location"),
                                // Flexible(child: WidgetText.subHeader(this._address1)),
                                // WidgetText.paragraph(
                                //     "Debug Version ~ Mock Location :" +
                                //         mockStatus.toString(),
                                //     false,
                                //     false)
                              ],
                            ),
                          ))
                  ],
                ),
              )),
          Expanded(child: _children[_currentIndex]),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   // onPressed: _incrementCounter,
      //   tooltip: 'Update Kehadiran',
      //   onPressed: () {
      //     return false;
      //   },
      //   child: Icon(Icons.add),
      // ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          (data['role_id'] == '5' ||
                  data['role_id'] == '6' ||
                  data['role_id'] == '13' ||
                  data['role_id'] == '15' ||
                  data['role_id'] == '19' ||
                  data['role_id'] == "22")
              ? Transform(
                  transform: Matrix4.translationValues(
                    0.0,
                    _translateButton.value * 4.0,
                    0.0,
                  ),
                  child: absensi_marketing(),
                )
              : Container(),
          Transform(
            transform: Matrix4.translationValues(
              0.0,
              _translateButton.value * 3.0,
              0.0,
            ),
            child: absensi_karyawan(),
          ),
          Transform(
            transform: Matrix4.translationValues(
              0.0,
              _translateButton.value * 2.0,
              0.0,
            ),
            child: cuti(),
          ),
          Transform(
            transform: Matrix4.translationValues(
              0.0,
              _translateButton.value,
              0.0,
            ),
            child: kehadiran(),
          ),
          this.toggle()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        // currentIndex: 0, // this will be set when a new tab is tapped
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.list),
            label: "Absensi",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Akun",
          ),
        ],
      ),
    );
  }
}
