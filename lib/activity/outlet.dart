import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
// import 'package:absensi_dev/component/search.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';

class Outlet extends StatefulWidget {
  Outlet({Key key}) : super(key: key);

  @override
  _OutletState createState() => _OutletState();
}

class _OutletState extends State<Outlet> {
  SearchBar searchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController _scrollController = new ScrollController();
  List data;
  bool _isLoading = true;
  bool _isEmpty = true;
  bool _isMore = false;
  String query = "";
  int page = 1;
  int limit = 15;

  Widget getResult() {
    return (_isEmpty)
        ? Card(
            elevation: 3,
            child: Center(
              child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Fonts.subHeadings(
                          "Data Kosong", false, false, Colors.black),
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              this.query = "";
                            });
                            this.getData("");
                          },
                          child: Fonts.subHeadings(
                              "Reset", false, false, Colors.white))
                    ],
                  )),
            ),
          )
        : SingleChildScrollView(
            physics: ScrollPhysics(),
            controller: _scrollController,
            child: Column(
              children: [
                (query == "")
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.all(12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Fonts.subHeadings(
                                "Keyword : $query", false, true, Colors.green),
                            TextButton(
                                onPressed: () {
                                  setState(() {
                                    this.query = "";
                                  });
                                  this.getData("");
                                },
                                child: Fonts.paragraph(
                                    "reset", false, false, Colors.blue))
                          ],
                        )),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        // print(data[index]["iId"]);
                        // Toast.show(data[index]['vNama'], context,
                        //     duration: 3, gravity: Toast.CENTER);
                        Map returning = {
                          'nama': data[index]['vNama'],
                          'code': data[index]['vCode'],
                          'id': data[index]['iId'],
                        };
                        Navigator.pop(context, returning);
                      },
                      child: Card(
                        elevation: 3,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: [
                              Fonts.subHeadings((index + 1).toString(), true,
                                  false, Colors.blueGrey),
                              SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Fonts.subHeadings(data[index]["vNama"], false,
                                      false, Colors.black),
                                  Fonts.paragraph(data[index]["vCode"], false,
                                      false, Colors.grey)
                                ],
                              ))
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: data.length,
                )
              ],
            ));
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        title: Text("Outlet"),
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context, false);
            return false;
          },
        ),
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    // Toast.show("Pencarian dengan keyword $value", context,
    //     duration: 2, gravity: Toast.CENTER);
    setState(() {
      this.query = value;
    });
    this.getData(value);
  }

  _OutletState() {
    searchBar = new SearchBar(
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted,
        onCleared: () {
          print("cleared");
        },
        onClosed: () {
          print("closed");
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Outlet"),
      //   leading: BackButton(
      //     onPressed: () {
      //       Navigator.pop(context, false);
      //       return false;
      //     },
      //   ),
      //   actions: [
      //     IconButton(
      //         icon: Icon(Icons.search),
      //         onPressed: () {
      //           showSearch(context: context, delegate: Search(widget.list));
      //         })
      //   ],
      // ),
      appBar: searchBar.build(context),
      key: _scaffoldKey,
      body: (_isLoading)
          ? Components.loadingProcess("Loading ...")
          : WillPopScope(
              onWillPop: () {
                Navigator.pop(context, false);
              },
              child: this.getResult(),
            ),
    );
  }

  @override
  void initState() {
    super.initState();
    // var clas = new Components();
    this.getData("");
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          this.page = this.page + 1;
        });
        getMore(this.page);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<Map> getData(String query) async {
    try {
      var offset = page - 1;
      var req = new Request();

      var res = await req
          .getRequest(Routing.outlet + '?offset=$offset&limit=$limit&q=$query');

      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              data = res['data'];
              _isEmpty = false;
            } else {
              _isEmpty = true;
            }
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  void getMore(page) async {
    if (!_isMore) {
      setState(() {
        this._isMore = true;
      });
    }

    if (this._isMore) {
      this._showMyDialog(context, "Loading");
    }

    int nextpage = page - 1;
    int offset = (limit * nextpage) + 1;

    var req = new Request();
    var res = await req
        .getRequest(Routing.outlet + '?offset=$offset&limit=$limit&q=');

    if (res['data'].isNotEmpty) {
      if (this.mounted) {
        setState(() {
          data.addAll(res['data']);
          _isMore = false;
          Navigator.of(context).pop();
        });
      }
    }
  }

  Future<bool> _showMyDialog(context, msg) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          // title: Text('Informasi'),
          content: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(6),
              child: Row(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    width: 8,
                  ),
                  Fonts.paragraph("Loading ...", false, false, Colors.black)
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
