import 'package:tps/activity/home.dart';
import 'package:tps/activity/login.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:toast/toast.dart';
import 'package:trust_location/trust_location.dart';
import 'package:location_permissions/location_permissions.dart';

class Splashscreen extends StatefulWidget {
  Splashscreen({Key key}) : super(key: key);

  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  bool mockStatus = false;
  bool loadLocation = false;
  String mockLocation = "";
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    requestLocationPermission();
    getLocation();
    Future.delayed(const Duration(seconds: 5), () {
      if (this.loadLocation == false) {
        TrustLocation.stop();
        if (this.mockStatus == true) {
          this._showMyDialog(context);
        } else {
          this.init();
        }
      }
    });
  }

  void requestLocationPermission() async {
    PermissionStatus permission =
        await LocationPermissions().requestPermissions();
    print('permissions: $permission');
  }

  Future<void> getLocation() async {
    try {
      TrustLocation.start(1);
      TrustLocation.onChange.listen((values) => setState(() {
            // _latitude = values.latitude;
            // _longitude = values.longitude;
            mockStatus = values.isMockLocation;
            this.mockLocation = values.isMockLocation.toString();
            this.loadLocation = false;
          }));
    } on PlatformException catch (e) {
      print('PlatformException $e');
    }
  }

  // @override
  // dispose() {
  //   TrustLocation.stop();
  //   super.dispose();
  // }

  Future<bool> _showMyDialog(context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Perhatian'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    "Anda Terdeteksi Menggunakan Lokasi Palsu, silahkan matikan lokasi palsu dan uninstall aplikasi lokasi palsu"),
                // Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Mengerti'),
              onPressed: () async {
                try {
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                } catch (e) {
                  Toast.show(e.toString(), context,
                      duration: 2, gravity: Toast.CENTER);
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future init() async {
    List<String> keys = ['uuid'];
    Map storage = await Storage.getStorage(keys);
    setState(() {
      if (storage['uuid'].isNotEmpty) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return Home();
        }));
      } else {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return Login();
        }));
      }
    });
    // Toast.show(storage.toString(), context, duration: 3, gravity: Toast.CENTER);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 130, 10, 0),
        width: double.infinity,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.gps_fixed_outlined,
                size: 60,
                color: Colors.amberAccent,
              ),
              SizedBox(
                height: 10,
              ),
              Fonts.headings("Loading", true, Colors.blueGrey),
              SizedBox(
                height: 10,
              ),
              Fonts.subHeadings(this.mockLocation, false, true, Colors.blue)
            ],
          ),
        ),
      ),
    );
  }
}
