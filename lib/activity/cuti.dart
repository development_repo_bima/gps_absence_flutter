import 'package:tps/activity/form_cuti.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class Cuti extends StatefulWidget {
  Cuti({Key key}) : super(key: key);

  @override
  _CutiState createState() => _CutiState();
}

class _CutiState extends State<Cuti> {
  bool _isLoading = true;
  bool _loadChild = false;
  Map data;
  List result;
  String _lokasi = "-";
  String flag;
  String total_cuti = 'Loading..';

  @override
  void initState() {
    super.initState();
    this.init();
    this.getDetail();
    // this._isLoading = false;
  }

  Future<void> getDetail() async {
    try {
      var req = new Request();

      var res = await req.getRequest(Routing.jumlah_cuti);
      // Toast.show(res.toString(), context, duration: 3, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              this.total_cuti = res['data']['total_cuti'].toString();
            } else {
              // _isEmpty = true;
            }
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      Toast.show(e.toString() + 'catch', context,
          duration: 3, gravity: Toast.CENTER);
    }
  }

  Future init() async {
    List<String> keys = [
      'uuid',
      'nip',
      'nama',
      'role',
      'role_id',
      'email',
      'kantor'
    ];
    Map storage = await Storage.getStorage(keys);
    setState(() {
      this.data = storage;
      this._lokasi = (storage['kantor'] == "PHC")
          ? "PT Prima Health Care"
          : "PT Total Medika Persada";
      this._isLoading = false;
    });
    // Toast.show(storage.toString(), context, duration: 3, gravity: Toast.CENTER);
  }

  Future<void> getHist(flag) async {
    try {
      setState(() {
        _loadChild = true;
      });
      var req = new Request();

      var res = await req.getRequest(Routing.historycuti + '?flag=$flag');
      // Toast.show(res.toString(), context, duration: 3, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            result = res['data'];
            this.flag = res['flag'];
            _loadChild = false;
          });
        }
        // Toast.show(result.toString(), context,
        //     duration: 3, gravity: Toast.CENTER);
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [],
        title: Text("Cuti Karyawan"),
      ),
      body: (_isLoading)
          ? Components.loadingProcess("Loading ...")
          : Container(
              padding: EdgeInsets.only(top: 10),
              width: double.infinity,
              child: Container(
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    Card(
                      elevation: 3,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(40, 1, 40, 20),
                        child: Column(
                          children: [
                            Fonts.headings("Cuti Tahunan", true, Colors.black),
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: DataTable(
                                  columns: <DataColumn>[
                                    DataColumn(label: Text("")),
                                    DataColumn(label: Text("")),
                                    DataColumn(label: Text("")),
                                  ],
                                  dataRowHeight: 40,
                                  columnSpacing: 10,
                                  rows: <DataRow>[
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Karyawan",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['nama']) ?? '-',
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph(
                                            "NIP", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['nip']) ?? '-',
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Area Kerja",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Row(
                                          children: [
                                            Expanded(
                                                child: Fonts.paragraph(
                                                    (this._lokasi) ?? '-',
                                                    false,
                                                    false,
                                                    Colors.black)),
                                          ],
                                        )),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Jabatan",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Row(
                                          children: [
                                            Expanded(
                                                child: Fonts.paragraph(
                                                    (data['role']) ?? '-',
                                                    false,
                                                    false,
                                                    Colors.black))
                                          ],
                                        )),
                                      ],
                                    ),
                                    // DataRow(
                                    //   cells: <DataCell>[
                                    //     DataCell(Fonts.paragraph(
                                    //         "Tanggal Masuk",
                                    //         false,
                                    //         false,
                                    //         Colors.black)),
                                    //     DataCell(Fonts.paragraph(
                                    //         ":", false, false, Colors.black)),
                                    //     DataCell(Fonts.paragraph("2020-12-01",
                                    //         false, false, Colors.black)),
                                    //   ],
                                    // ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Sisa Cuti",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            this.total_cuti,
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                  ]),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  width: 90,
                                  child: InkWell(
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.red,
                                          onPrimary: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(12))),
                                      onLongPress: () {
                                        Toast.show("Cuti Digunakan", context,
                                            duration: 2, gravity: Toast.CENTER);
                                      },
                                      onPressed: () {
                                        this.getHist("cuti_used");
                                      },
                                      child: Column(
                                        children: [
                                          Icon(
                                            Icons.schedule_send,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 90,
                                  child: InkWell(
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.blue,
                                          onPrimary: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(12))),
                                      onLongPress: () {
                                        Toast.show("Cuti Terbit", context,
                                            duration: 2, gravity: Toast.CENTER);
                                      },
                                      onPressed: () {
                                        this.getHist("cuti_terbit");
                                      },
                                      child: Icon(
                                        Icons.approval,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 90,
                                  child: InkWell(
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.green,
                                          onPrimary: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(12))),
                                      onLongPress: () {
                                        Toast.show(
                                            "Input Pengajuan Cuti", context,
                                            duration: 2, gravity: Toast.CENTER);
                                      },
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    FormCuti()));
                                      },
                                      child: Icon(
                                        Icons.create,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    (this.flag == null)
                        ? Container()
                        : Card(
                            elevation: 3,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 1, 10, 1),
                              child: (_loadChild)
                                  ? Components.loadingProcess("Memuat Data ...")
                                  : (this.flag == "cuti_terbit")
                                      ? Column(
                                          children: [
                                            Fonts.headings("Cuti Terbit", true,
                                                Colors.black),
                                            SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                child: DataTable(
                                                    columns: <DataColumn>[
                                                      DataColumn(
                                                          label:
                                                              Text("Tanggal")),
                                                      DataColumn(
                                                          label:
                                                              Text("Semester")),
                                                      DataColumn(
                                                          label:
                                                              Text("Jumlah")),
                                                    ],
                                                    rows: result
                                                        .map((e) =>
                                                            DataRow(cells: [
                                                              DataCell(Fonts.paragraph(
                                                                  e['tgl_terbit'],
                                                                  false,
                                                                  false,
                                                                  Colors.black)),
                                                              DataCell(Fonts.paragraph(
                                                                  e['semester'],
                                                                  false,
                                                                  false,
                                                                  Colors
                                                                      .black)),
                                                              DataCell(Fonts.paragraph(
                                                                  e['jumlah']
                                                                      .toString(),
                                                                  false,
                                                                  false,
                                                                  Colors
                                                                      .black)),
                                                            ]))
                                                        .toList())),
                                          ],
                                        )
                                      : Column(
                                          children: [
                                            Fonts.headings("Cuti Digunakan",
                                                true, Colors.black),
                                            SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                child: DataTable(
                                                    columns: <DataColumn>[
                                                      DataColumn(
                                                          label:
                                                              Text("Tgl Cuti")),
                                                      DataColumn(
                                                          label: Text(
                                                              "Keterangan")),
                                                      DataColumn(
                                                          label:
                                                              Text("Jumlah")),
                                                    ],
                                                    rows: result
                                                        .map(
                                                            (e) => DataRow(
                                                                    cells: [
                                                                      DataCell(Fonts.paragraph(
                                                                          e['tgl_cuti'],
                                                                          false,
                                                                          false,
                                                                          Colors.black)),
                                                                      DataCell(Fonts.paragraph(
                                                                          e['text'],
                                                                          false,
                                                                          false,
                                                                          Colors.black)),
                                                                      DataCell(Fonts.paragraph(
                                                                          e['jumlah']
                                                                              .toString(),
                                                                          false,
                                                                          false,
                                                                          Colors
                                                                              .black)),
                                                                    ]))
                                                        .toList())),
                                          ],
                                        ),
                            ),
                          )
                  ],
                )),
              ),
            ),
    );
  }
}
