import 'dart:convert';
import 'dart:typed_data';

import 'package:tps/activity/home.dart';
import 'package:tps/activity/login.dart';
import 'package:tps/activity/product.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:signature/signature.dart';
import 'package:toast/toast.dart';

class Kunjungan extends StatefulWidget {
  final String flag;
  final String id;

  Kunjungan({Key key, this.flag, this.id}) : super(key: key);
  @override
  _KunjunganState createState() => _KunjunganState();
}

class _KunjunganState extends State<Kunjungan> {
  bool _isLoading = true;
  Uint8List signPng;
  List<dynamic> listProduct = [];
  String outletNama,
      outletId,
      outletCode,
      typeString,
      ruanganString,
      _lokasi,
      _address,
      idHeader,
      signBlob;
  String textPic, rolePic, commentPic, commentRep;
  List type = ["Home Visit", "Direct Call"];
  List ruangan = [
    "ICU",
    "NICU",
    "PICU",
    "ICCU",
    "HCU",
    "OT/OK",
    "IGD",
    "IPSRS",
    "Purchasing Logistik",
    "Purchasing Farmasi"
  ];

  Map data;
  bool _isEmpty = true;
  bool _isMarketing = false;
  String _postButton = "Kirim";

  final SignatureController _controllerPIC = SignatureController(
    penStrokeWidth: 1,
    penColor: Colors.red,
    exportBackgroundColor: Colors.blue,
  );

  final SignatureController _controllerMED = SignatureController(
    penStrokeWidth: 1,
    penColor: Colors.red,
    exportBackgroundColor: Colors.blue,
  );

  @override
  void initState() {
    this._isLoading = true;
    this.getDetail();

    _controllerPIC.addListener(() => print('Value changed'));
    _controllerMED.addListener(() => print('Value changed'));
  }

  Future init() async {
    var key = 'blob_' + this.idHeader;
    List<String> keys = [key];
    Map storage = await Storage.getStorage(keys);
    setState(() {
      if (storage[key] != null || storage[key] != "") {
        this.signPng = base64Decode(storage[key]);
        this.signBlob = storage[key];
      }
    });
  }

  Future<Map> getDetail() async {
    try {
      var req = new Request();
      String iId = (widget.id.isEmpty) ? '0' : widget.id;
      String flags = (widget.flag.isEmpty) ? '' : widget.flag;
      var res = await req
          .getRequest(Routing.detail_kunjungan + '?id=$iId&flag=$flags');
      // Toast.show(res.toString(), context, duration: 2, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              // this.title = "Detail " + (res['jenis']) ?? '-';
              data = res['data'];
              _isEmpty = false;
              this._isMarketing = (res['jenis'] == "Kunjungan") ? true : false;

              this.outletCode = res['data']['code'];
              this.outletId = res['data']['id_outlet'].toString();
              this.outletNama = res['data']['nama'];
              this._lokasi = res['data']['vLokasi'];
              this._address = res['data']['address'];
              this.idHeader = res['data']['id'];
            } else {
              _isEmpty = true;
            }
            _isLoading = false;
          });
          this.init();
        }
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  Future postData(json) async {
    try {
      var req = new Request();
      // Toast.show(Routing.report_marketing, context,
      //     duration: 3, gravity: Toast.CENTER);
      var post = await req.postRequest(Routing.report_marketing, body: json);
      // Toast.show(post.toString(), context, duration: 7, gravity: Toast.CENTER);
      if (!post['auth_validation']) {
        Toast.show("Sesi Login anda sudah habis, silahkan login ulang", context,
            duration: 2, gravity: Toast.CENTER);

        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => Login()),
          (Route<dynamic> route) => false,
        );
      } else if (post['status']) {
        Toast.show(post['msg'], context, duration: 2, gravity: Toast.CENTER);

        Future.delayed(const Duration(seconds: 2), () {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => Home()),
            (Route<dynamic> route) => false,
          );
        });
      } else {
        Toast.show(post['msg'], context, duration: 2, gravity: Toast.CENTER);
      }
      setState(() {
        this._postButton = "Kirim";
      });
    } catch (e) {
      Toast.show("e", context, duration: 3, gravity: Toast.CENTER);
      setState(() {
        this._postButton = "Kirim";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Fonts.headings("Kunjungan", false, Colors.white),
      ),
      body: (_isLoading)
          ? Components.loadingProcess("Loading ...")
          : Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              width: double.infinity,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ExpansionTile(
                        title: Fonts.subHeadings(
                            "Info Outlet", false, false, Colors.black),
                        children: [
                          Container(
                              height: 80.0,
                              width: 700.0,
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade300,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: Padding(
                                padding: EdgeInsets.all(8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Fonts.paragraph("Info Outlet", false, false,
                                        Colors.black),
                                    Fonts.paragraph((this.outletNama) ?? '-',
                                        true, false, Colors.blue),
                                    Fonts.subParagraph((this.outletCode) ?? '-',
                                        false, true, Colors.black)
                                  ],
                                ),
                              )),
                          SizedBox(
                            height: 12,
                          ),
                          Container(
                              height: 100.0,
                              width: 700.0,
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade300,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: Padding(
                                padding: EdgeInsets.all(8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Fonts.paragraph(
                                        "Lokasi", false, false, Colors.black),
                                    Fonts.subParagraph(
                                        (this._lokasi) + ", " + this._address,
                                        false,
                                        true,
                                        Colors.black),
                                  ],
                                ),
                              )),
                        ]),
                    SizedBox(
                      height: 9,
                    ),
                    DropdownButton<String>(
                      isExpanded: true,
                      hint: Text("Pilih Jenis Kunjungan"),
                      value: typeString,
                      items: type.map((value) {
                        return DropdownMenuItem<String>(
                          child: Fonts.paragraph(
                              value, false, false, Colors.black),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (val) {
                        setState(() {
                          typeString =
                              val; //Untuk memberitahu _valFriends bahwa isi nya akan diubah sesuai dengan value yang kita pilih
                        });
                      },
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    DropdownButton<String>(
                      isExpanded: true,
                      hint: Text("Pilih Ruangan/Bangsal"),
                      value: ruanganString,
                      items: ruangan.map((value) {
                        return DropdownMenuItem<String>(
                          child: Fonts.paragraph(
                              value, false, false, Colors.black),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (val) {
                        setState(() {
                          ruanganString =
                              val; //Untuk memberitahu _valFriends bahwa isi nya akan diubah sesuai dengan value yang kita pilih
                        });
                      },
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    ExpansionTile(
                      title: Fonts.subHeadings(
                          "Data PIC", false, true, Colors.black),
                      children: [
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Nama PIC'),
                          onChanged: (v) {
                            setState(() {
                              textPic = v;
                            });
                          },
                        ),
                        SizedBox(
                          height: 9,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Jabatan PIC'),
                          onChanged: (v) {
                            setState(() {
                              this.rolePic = v;
                            });
                          },
                        ),
                        SizedBox(
                          height: 9,
                        ),
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'Komentar PIC'),
                          onChanged: (v) {
                            setState(() {
                              this.commentPic = v;
                            });
                          },
                        ),
                        SizedBox(
                          height: 9,
                        ),
                        Fonts.paragraph("TTD PIC", false, false, Colors.black),
                        (this.signPng.length > 0)
                            ? Container(
                                height: 170,
                                width: 250,
                                child: Center(
                                  child: Container(
                                      color: Colors.grey[300],
                                      child: Image.memory(this.signPng)),
                                ),
                              )
                            : Signature(
                                controller: _controllerPIC,
                                height: 170,
                                width: 250,
                                backgroundColor: Colors.black54,
                              ),
                        (this.signPng.length > 0)
                            ? Container()
                            : Container(
                                height: 40,
                                width: 250,
                                decoration:
                                    const BoxDecoration(color: Colors.indigo),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  // mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    //SHOW EXPORTED IMAGE IN NEW ROUTE
                                    IconButton(
                                      icon: const Icon(Icons.check),
                                      color: Colors.blue,
                                      onPressed: () async {
                                        if (_controllerPIC.isNotEmpty) {
                                          final Uint8List data =
                                              await _controllerPIC.toPngBytes();

                                          setState(() {
                                            this.signBlob = base64Encode(data);
                                            this.signPng = data;
                                          });
                                          List<String> key = [
                                            'blob_' + this.idHeader
                                          ];
                                          List<String> val = [this.signBlob];
                                          await Storage.setStorage(key, val);
                                        }
                                      },
                                    ),
                                    //CLEAR CANVAS
                                    IconButton(
                                      icon: const Icon(Icons.clear),
                                      color: Colors.blue,
                                      onPressed: () {
                                        setState(() => _controllerPIC.clear());
                                      },
                                    ),
                                  ],
                                ),
                              ),
                      ],
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    ExpansionTile(
                      title: Fonts.subHeadings(
                          "Data Medrep", false, true, Colors.black),
                      children: [
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'Komentar Medrep'),
                          onChanged: (v) {
                            setState(() {
                              this.commentRep = v;
                            });
                          },
                        ),
                        SizedBox(
                          height: 9,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    ExpansionTile(
                        title: Fonts.subHeadings(
                            "Data Produk", false, true, Colors.black),
                        children: [
                          TextButton(
                              onPressed: () async {
                                var product = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Product()));
                                setState(() {
                                  if (product != false) {
                                    this.listProduct.addAll(product);
                                  }
                                });
                              },
                              child: Fonts.paragraph(
                                  "Pilih Produk", false, false, Colors.black)),
                          (listProduct.length > 1)
                              ? TextButton(
                                  onPressed: () {
                                    setState(() {
                                      listProduct.clear();
                                    });
                                  },
                                  child: Fonts.subParagraph("Reset List Produk",
                                      false, false, Colors.blue))
                              : Container(),
                          Container(
                            height: 150,
                            child: (listProduct.length < 1)
                                ? Container()
                                : ListView.builder(
                                    itemBuilder: (context, index) {
                                      return Card(
                                          elevation: 0,
                                          child: Padding(
                                            padding: EdgeInsets.all(8),
                                            child: Row(
                                              children: [
                                                Fonts.paragraph(
                                                    (index + 1).toString(),
                                                    false,
                                                    false,
                                                    Colors.black),
                                                SizedBox(
                                                  width: 6,
                                                ),
                                                Expanded(
                                                    child: Fonts.paragraph(
                                                        listProduct[index]
                                                            ['nama'],
                                                        false,
                                                        false,
                                                        Colors.black))
                                              ],
                                            ),
                                          ));
                                    },
                                    itemCount: listProduct.length,
                                  ),
                          ),
                        ]),
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      width: 450,
                      child: ElevatedButton(
                          style:
                              ElevatedButton.styleFrom(onPrimary: Colors.blue),
                          child: Fonts.subHeadings(
                              "Kirim", false, false, Colors.white),
                          onPressed: () {
                            try {
                              if (typeString == null) {
                                Toast.show("Pilih Jenis Kunjungan", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else if (ruanganString == null) {
                                Toast.show("Pilih Ruangan", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else if (textPic == null) {
                                Toast.show(
                                    "Nama PIC tidak boleh kosong", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else if (rolePic == null) {
                                Toast.show(
                                    "Jabatan PIC tidak boleh kosong", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else if (commentPic == null) {
                                Toast.show(
                                    "Komentar PIC tidak boleh kosong", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else if (commentRep == null) {
                                Toast.show(
                                    "Komentar SR tidak boleh kosong", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else if (listProduct == null) {
                                Toast.show(
                                    "Data Product tidak boleh kosong", context,
                                    duration: 3, gravity: Toast.CENTER);
                              } else {
                                var post = {
                                  'id_header': idHeader,
                                  'id_outlet': outletId,
                                  'type': typeString,
                                  'ruangan': ruanganString,
                                  'pic': textPic,
                                  'role_pic': rolePic,
                                  'comment_pic': commentPic,
                                  'comment_rep': commentRep,
                                  'list_product': listProduct.toString(),
                                  'encode_img': (this.signBlob == null)
                                      ? "-"
                                      : this.signBlob,
                                };
                                this._postButton = "Loading ...";
                                this.postData(post);
                                // Toast.show(post.toString(), context,
                                //     duration: 3, gravity: Toast.CENTER);
                              }
                            } catch (e) {
                              Toast.show(e.toString(), context,
                                  duration: 3, gravity: Toast.CENTER);
                            }
                          }),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
