import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
// import 'package:absensi_dev/component/search.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:intl/intl.dart';

class Product extends StatefulWidget {
  Product({Key key}) : super(key: key);

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  ScrollController _scrollController = new ScrollController();
  SearchBar searchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List data;
  List<dynamic> _choose = List();
  bool _isLoading = true;
  bool _isEmpty = true;
  bool _isMore = false;
  bool _isScrollable = true;
  String query = "";
  int page = 1;
  int limit = 15;
  int counter = 0;
  final format = NumberFormat("#,##0.00", "en_US");

  Widget getResult() {
    return (_isEmpty)
        ? Card(
            elevation: 3,
            child: Center(
              child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Fonts.subHeadings(
                          "Data Kosong", false, false, Colors.black),
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              this.query = "";
                            });
                            this.getData();
                          },
                          child: Fonts.subHeadings(
                              "Reset", false, false, Colors.white))
                    ],
                  )),
            ),
          )
        : SingleChildScrollView(
            physics: ScrollPhysics(),
            controller: _scrollController,
            child: Column(
              children: [
                (query == "")
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.all(12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Fonts.subHeadings(
                                "Keyword : $query", false, true, Colors.green),
                            TextButton(
                                onPressed: () {
                                  setState(() {
                                    this.query = "";
                                  });
                                  this.getData();
                                },
                                child: Fonts.paragraph(
                                    "reset", false, false, Colors.blue))
                          ],
                        )),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: Card(
                          elevation: 3,
                          child: InkWell(
                            onTap: () {
                              // Toast.show(_choose.length.toString(), context,
                              //     duration: 2, gravity: Toast.CENTER);
                              setState(() {
                                this._choose.add({
                                  'id': data[index]['iId'],
                                  'nama': data[index]['vNama']
                                });
                                this.counter = this._choose.length;
                              });
                              // Toast.show(_choose.toString(), context,
                              //     duration: 2, gravity: Toast.CENTER);
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  Fonts.subHeadings((index + 1).toString(),
                                      true, false, Colors.blueGrey),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Fonts.subHeadings(
                                          (data[index]["vNama"]) ?? '-',
                                          false,
                                          false,
                                          Colors.black),
                                      Fonts.paragraph(
                                          (data[index]["vCode"]) ?? '-',
                                          false,
                                          false,
                                          Colors.grey),
                                      Fonts.paragraph(
                                          'Rp. ' +
                                                  format
                                                      .format(int.parse(
                                                          data[index]
                                                                  ["dTotal"] ??
                                                              '0'))
                                                      .toString() +
                                                  ' /' +
                                                  data[index]['vKemasan'] ??
                                              '-',
                                          false,
                                          false,
                                          Colors.grey),
                                    ],
                                  ))
                                ],
                              ),
                            ),
                          )),
                    );
                  },
                  itemCount: data.length,
                )
              ],
            ),
          );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        title: Text("Produk"),
        leading: BackButton(
          onPressed: () {
            // Toast.show(_choose.length.toString(), context,
            //     duration: 2, gravity: Toast.CENTER);
            Navigator.pop(context, false);
            return false;
          },
        ),
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    setState(() {
      this.query = value;
    });
    this.getData();
    // setState(() => _scaffoldKey.currentState
    //     .showSnackBar(new SnackBar(content: new Text('You wrote $value!'))));
  }

  _ProductState() {
    searchBar = new SearchBar(
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted,
        onCleared: () {},
        onClosed: () {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Produk"),
      //   leading: BackButton(
      //     onPressed: () {
      //       Navigator.pop(context, false);
      //       return false;
      //     },
      //   ),
      //   actions: [
      //     IconButton(
      //         icon: Icon(Icons.search),
      //         onPressed: () async {
      //           final String q = await showSearch(
      //               context: context, delegate: Search(widget.list));

      //           Toast.show(q, context, duration: 3, gravity: Toast.CENTER);
      //         })
      //   ],
      // ),

      appBar: searchBar.build(context),
      key: _scaffoldKey,
      body: (_isLoading)
          ? Components.loadingProcess("Loading ...")
          : WillPopScope(
              child: this.getResult(),
              onWillPop: () {
                Navigator.pop(context, false);
              }),
      floatingActionButton: (counter < 1)
          ? Container()
          : FloatingActionButton(
              onPressed: () {
                Navigator.pop(context, this._choose);
              },
              child: Stack(
                children: [
                  Icon(Icons.send_and_archive),
                  Positioned(
                    right: 11,
                    top: 11,
                    child: new Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 14,
                        minHeight: 14,
                      ),
                      child: Text(
                        this.counter.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
              backgroundColor: Colors.cyan,
            ),
    );
  }

  @override
  void initState() {
    super.initState();
    // var clas = new Components();
    this.getData();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          this.page = this.page + 1;
        });
        if (this._isScrollable) {
          getMore(this.page);
        }
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<Map> getData() async {
    try {
      var offset = page - 1;
      var req = new Request();

      var res = await req.getRequest(
          Routing.product + '?offset=$offset&limit=$limit&q=$query');
      // Toast.show(res.toString(), context, duration: 3, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              data = res['data'];
              _isEmpty = false;
            } else {
              _isEmpty = true;
            }
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  void getMore(page) async {
    if (!_isMore) {
      setState(() {
        this._isMore = true;
      });
    }

    if (this._isMore) {
      this._showMyDialog(context, "Loading");
    }

    int nextpage = page - 1;
    int offset = (limit * nextpage) + 1;

    var req = new Request();
    var res = await req
        .getRequest(Routing.product + '?offset=$offset&limit=$limit&q=');

    if (res['data'].isNotEmpty) {
      if (this.mounted) {
        setState(() {
          data.addAll(res['data']);
          _isMore = false;
          Navigator.of(context).pop();
        });
      }
    } else {
      setState(() {
        this._isScrollable = false;
      });
      Toast.show("Telah mencapai limit", context,
          duration: 3, gravity: Toast.CENTER);
      Navigator.of(context).pop();
    }
  }

  Future<bool> _showMyDialog(context, msg) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          // title: Text('Informasi'),
          content: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(6),
              child: Row(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    width: 8,
                  ),
                  Fonts.paragraph("Loading ...", false, false, Colors.black)
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
