import 'package:tps/activity/home.dart';
import 'package:tps/activity/login.dart';
import 'package:tps/activity/outlet.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:toast/toast.dart';
import 'package:trust_location/trust_location.dart';

class Checkin extends StatefulWidget {
  Checkin({Key key}) : super(key: key);

  @override
  _CheckinState createState() => _CheckinState();
}

class _CheckinState extends State<Checkin> {
  bool _isLoading = true;
  bool mockStatus = false;
  bool loadLocation = false;
  String _keterangan, _tagLokasi;
  String _choosingLocation = "-";
  Position _currentPosition;
  var txt = TextEditingController();

  String outletNama, outletId, outletCode;

  List _absensi = ["Check In", "Check Out"];
  List<Address> results = [];
  List _locationString = ["-"];

  Widget checkForm;
  String _postButton = "Post";

  String _latitude;
  String _longitude;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  /// get location method, use a try/catch PlatformException.
  Future<void> getLocation() async {
    try {
      TrustLocation.onChange.listen((values) => setState(() {
            _latitude = values.latitude;
            _longitude = values.longitude;
            mockStatus = values.isMockLocation;
          }));
    } on PlatformException catch (e) {
      print('PlatformException $e');
    }
  }

  _getCurrentLocation() {
    setState(() {
      loadLocation = true;
    });
    // requestLocationPermission();

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        // _params.add({'latitude': _currentPosition.latitude},
        //     {'longitude': _currentPosition.longitude});
      });

      _getAddressFromLatLng();
      TrustLocation.start(3);
      setState(() {
        if (this.mockStatus == true) {
          this._showMyDialog(context);
        }
      });
      setState(() {
        _isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      final coordinates =
          Coordinates(_currentPosition.latitude, _currentPosition.longitude);

      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);

      for (var i = 0; i < addresses.length; i++) {
        String line = "${addresses[i].addressLine}";
        // print(line);
        setState(() {
          _locationString.add(line);
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
    getLocation();
  }

  Future postData(json) async {
    try {
      var req = new Request();
      // Toast.show(json.toString(), context, duration: 3, gravity: Toast.CENTER);
      Map<String, dynamic> post =
          await req.postRequest(Routing.post_marketing, body: json);
      // Toast.show(post.toString(), context, duration: 8, gravity: Toast.CENTER);
      if (!post['auth_validation']) {
        Toast.show("Sesi Login anda sudah habis, silahkan login ulang", context,
            duration: 2, gravity: Toast.CENTER);

        // Navigator.pushReplacement(context,
        //     MaterialPageRoute(builder: (BuildContext context) {
        //   return Login();
        // }));
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => Login()),
          (Route<dynamic> route) => false,
        );
      } else if (post['status']) {
        Toast.show(post['msg'], context, duration: 2, gravity: Toast.CENTER);

        // Navigator.pushReplacement(context,
        //     MaterialPageRoute(builder: (BuildContext context) {
        //   return Home();
        // }));
        Future.delayed(const Duration(seconds: 2), () {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => Home()),
            (Route<dynamic> route) => false,
          );
        });
      } else {
        Toast.show(post['msg'], context, duration: 2, gravity: Toast.CENTER);
      }
      setState(() {
        this._postButton = "Post";
      });
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
      setState(() {
        this._postButton = "Post";
      });
    }
  }

  Future<bool> _showMyDialog(context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Perhatian'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    "Anda Terdeteksi Menggunakan Lokasi Palsu, silahkan matikan lokasi palsu dan uninstall aplikasi lokasi palsu"),
                // Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            // TextButton(
            //   child: Text('Batal'),
            //   onPressed: () {
            //     Navigator.of(context).pop();
            //   },
            // ),
            TextButton(
              child: Text('Mengerti'),
              onPressed: () async {
                try {
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                } catch (e) {
                  Toast.show(e.toString(), context,
                      duration: 2, gravity: Toast.CENTER);
                }
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Fonts.headings("Absensi Marketing", false, Colors.white),
        ),
        body: (_isLoading)
            ? Components.loadingProcess("Mendapatkan Lokasi Anda ...")
            : Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                // height: 220,
                width: double.infinity,
                // width: double.maxFinite,
                child: SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(4),
                      child: Center(
                        child: Fonts.subHeadings(
                            "Check In / Out", true, false, Colors.black),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      elevation: 5,
                      child: Padding(
                          padding: EdgeInsets.all(7),
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                TextButton(
                                    onPressed: () async {
                                      var result = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Outlet()));
                                      if (result != false) {
                                        setState(() {
                                          this.outletCode = result['code'];
                                          this.outletId =
                                              result['id'].toString();
                                          this.outletNama = result['nama'];
                                          txt.text = result['nama'];
                                          _tagLokasi = result['nama'];
                                        });
                                      }
                                    },
                                    child: Fonts.paragraph("Pilih Outlet",
                                        false, false, Colors.black)),
                                Container(
                                    height: 80.0,
                                    width: 350.0,
                                    decoration: BoxDecoration(
                                        color: Colors.grey.shade300,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0))),
                                    child: Padding(
                                        padding: EdgeInsets.all(8),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Fonts.paragraph("Info Outlet",
                                                false, false, Colors.black),
                                            Fonts.paragraph(
                                                (this.outletNama) ?? '-',
                                                true,
                                                false,
                                                Colors.blue),
                                            Fonts.subParagraph(
                                                (this.outletCode) ?? '-',
                                                false,
                                                true,
                                                Colors.black)
                                          ],
                                        ))),
                                SizedBox(
                                  height: 12,
                                ),
                                TextFormField(
                                  controller: txt,
                                  // initialValue: (_tagLokasi) ?? '-',
                                  decoration: InputDecoration(
                                      labelText: 'Keterangan Tempat'),

                                  onChanged: (val) {
                                    setState(() {
                                      _tagLokasi = val;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                DropdownButton<String>(
                                  isExpanded: true,
                                  hint: Text("Pilih Lokasi"),
                                  value: _choosingLocation,
                                  items: _locationString.map((value) {
                                    return DropdownMenuItem<String>(
                                      child: Fonts.paragraph(
                                          value, false, false, Colors.black),
                                      value: value,
                                    );
                                  }).toList(),
                                  onChanged: (val) {
                                    setState(() {
                                      _choosingLocation =
                                          val; //Untuk memberitahu _valFriends bahwa isi nya akan diubah sesuai dengan value yang kita pilih
                                    });
                                    val = "pilih";
                                  },
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Text(
                                  _choosingLocation,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                DropdownButton(
                                  hint: Text("Pilih Jenis Absensi"),
                                  value: _keterangan,
                                  items: _absensi.map((value) {
                                    return DropdownMenuItem(
                                      child: Fonts.paragraph(
                                          value, false, false, Colors.black),
                                      value: value,
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      _keterangan = value;
                                      //form_keterangan.text = value;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 12,
                                ),
                                Container(
                                  width: 450,
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      setState(() {
                                        this._postButton = "Loading ...";
                                      });
                                      var post = false;
                                      var _par = {};
                                      if (_choosingLocation == '-') {
                                        Toast.show("Pilih Lokasi Anda", context,
                                            duration: 2, gravity: Toast.CENTER);
                                        post = false;
                                      } else if (_keterangan == null) {
                                        Toast.show(
                                            "Pilih jenis absensi", context,
                                            duration: 2, gravity: Toast.CENTER);
                                        post = false;
                                      } else if (outletId == null) {
                                        Toast.show("Pilih Outlet", context,
                                            duration: 2, gravity: Toast.CENTER);
                                        post = false;
                                      } else {
                                        _par = {
                                          'longitude': _longitude,
                                          'latitude': _latitude,
                                          'address': _choosingLocation,
                                          'type': (_keterangan == null)
                                              ? "Update"
                                              : _keterangan,
                                          'tag': _tagLokasi,
                                          'outlet_id': (outletId == null)
                                              ? "-"
                                              : outletId,
                                        };

                                        // Toast.show(_par.toString(), context,
                                        //     duration: 8, gravity: Toast.CENTER);
                                        post = true;
                                      }

                                      if (post) {
                                        this.postData(_par);
                                      }
                                      setState(() {
                                        this._postButton = "Post";
                                      });
                                    },
                                    child: Fonts.subHeadings(this._postButton,
                                        false, false, Colors.white),
                                  ),
                                )
                              ],
                            ),
                          )),
                    ),
                  ],
                ))));
  }
}
