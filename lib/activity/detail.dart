import 'package:tps/activity/detail_kunjungan.dart';
import 'package:tps/activity/kunjungan.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class Detail extends StatefulWidget {
  final String flag;
  final String id;

  Detail({Key key, this.flag, this.id}) : super(key: key);

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  Map data;
  Map storage;
  bool _isLoading = true;
  bool _isUser = true;
  // bool _isEmpty = true;
  bool _isMarketing = false;
  bool _isDone = false;
  String title = '';
  Future<void> getDetail() async {
    try {
      var req = new Request();
      String uid = (widget.id.isEmpty) ? '0' : widget.id;
      String flags = (widget.flag.isEmpty) ? '' : widget.flag;
      var res =
          await req.getRequest(Routing.detail_inbox + '?id=$uid&flag=$flags');
      // Toast.show(flags, context, duration: 3, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              this.title = "Detail " + (res['jenis']) ?? '-';
              data = res['data'];
              // _isEmpty = false;

              this._isDone = (data['iLengkap'] == 1) ? true : false;

              if (res['jenis'] == "Kunjungan" &&
                  data['iMarketingId'].toString() !=
                      this.storage['id_karyawan'].toString()) {
                this._isUser = false;
              }
              this._isMarketing = (res['jenis'] == "Kunjungan") ? true : false;

              // Toast.show(_isUser.toString(), context,
              //     duration: 3, gravity: Toast.CENTER);
            } else {
              // _isEmpty = true;
            }
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  Future init() async {
    Map storage = await Storage.getStorage(['id_karyawan', 'role_id']);
    setState(() {
      this.storage = storage;
    });
    // Toast.show(this.storage['id_karyawan'], context,
    //     duration: 3, gravity: Toast.CENTER);
  }

  @override
  void initState() {
    super.initState();
    this.init();
    this.getDetail();
  }

  launchMap(String lat, String long) async {
    var mapSchema = 'geo:$lat,$long';
    if (await canLaunch(mapSchema)) {
      await launch(mapSchema);
    } else {
      throw 'Could not launch $mapSchema';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: (_isLoading)
          ? Components.loadingProcess("Loading ...")
          : Container(
              padding: EdgeInsets.only(top: 30),
              width: double.infinity,
              child: Container(
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    Card(
                      elevation: 3,
                      child: Padding(
                        padding: EdgeInsets.all(12),
                        child: Column(
                          children: [
                            Fonts.headings(this.title, true, Colors.black),
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: DataTable(
                                  columns: <DataColumn>[
                                    DataColumn(label: Text("")),
                                    DataColumn(label: Text("")),
                                    DataColumn(label: Text("")),
                                  ],
                                  dataRowHeight: 80,
                                  columnSpacing: 10,
                                  rows: <DataRow>[
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Waktu", false,
                                            false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['tCreated']) ?? '-',
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Lokasi",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            data['vLokasi'] +
                                                    ", " +
                                                    (data['tKeterangan']) ??
                                                '-',
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Pin Maps",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Row(
                                          children: [
                                            Fonts.paragraph(
                                                '(' +
                                                    data['vLatitude'] +
                                                    ', ' +
                                                    data['vLongitude'] +
                                                    ')',
                                                false,
                                                false,
                                                Colors.black),
                                            SizedBox(
                                              height: 8,
                                            ),
                                            IconButton(
                                                icon: Icon(
                                                  Icons.map,
                                                  color: Colors.redAccent,
                                                ),
                                                onPressed: () async {
                                                  // this.launchMap(
                                                  //     data['vLatitude'],
                                                  //     data['vLongitude']);
                                                  String url =
                                                      'http://maps.google.com/maps?q=' +
                                                          data['vLatitude'] +
                                                          ',' +
                                                          data['vLongitude'];

                                                  if (await canLaunch(url)) {
                                                    await launch(url);
                                                  } else {
                                                    Toast.show(
                                                        "Cannot Launch Maps",
                                                        context,
                                                        duration: 3,
                                                        gravity: Toast.CENTER);
                                                  }
                                                })
                                          ],
                                        )),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Pesan", false,
                                            false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['vText']) ?? '-',
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                  ]),
                            ),
                            (!this._isMarketing)
                                ? Container()
                                : (data['eType'] == 'CO')
                                    ? Container()
                                    : Container(
                                        width: 450,
                                        child: ElevatedButton(
                                          onPressed: () async {
                                            // Jika belum selesai dan user yang login adalah pic
                                            if (this._isDone == false &&
                                                this._isUser) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Kunjungan(
                                                            id: data['iId']
                                                                .toString(),
                                                            flag: data['flag']
                                                                .toString(),
                                                          )));
                                            } else if (!this._isUser &&
                                                this._isDone) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          DetailKunjungan(
                                                              id: data['iId']
                                                                  .toString())));
                                            } else if (!this._isUser &&
                                                !this._isDone) {
                                              Toast.show(
                                                  "Report Kunjungan Belum di isi oleh marketing",
                                                  context,
                                                  duration: 3,
                                                  gravity: Toast.CENTER);
                                            } else {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          DetailKunjungan(
                                                              id: data['iId']
                                                                  .toString())));
                                            }
                                          },
                                          child: Fonts.subHeadings(
                                              (!this._isDone)
                                                  ? "Isi Report Kunjungan"
                                                  : "Lihat Report Kunjungan",
                                              false,
                                              false,
                                              Colors.white),
                                        ),
                                      )
                          ],
                        ),
                      ),
                    )
                  ],
                )),
              ),
            ),
    );
  }
}
