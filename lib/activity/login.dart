import 'dart:async';
import 'dart:io';

import 'package:tps/activity/home.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/model/auth_model.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool show = true;
  IconData suffix = Icons.lightbulb_outline;
  String _username, _password;
  String textButton = "Masuk";
  List key;
  List value;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // this.textButton = "Masuk";
    this.init();
  }

  Future init() async {
    List<String> keys = ['uuid'];
    Map storage = await Storage.getStorage(keys);
    setState(() {
      if (storage['uuid'].isNotEmpty) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return Home();
        }));
      }
    });
    // Toast.show(storage.toString(), context, duration: 3, gravity: Toast.CENTER);
  }

  Future postData(json) async {
    try {
      var req = new Request();

      Map<String, dynamic> post =
          await req.postRequest(Routing.login, body: json);

      if (post['status']) {
        setState(() {
          this.textButton = "Masuk";
        });
        Map data = post['data'];

        List<String> keys = [
          'uuid',
          'nip',
          'nama',
          'id_karyawan',
          'role',
          'role_id',
          'email',
          'kantor'
        ];
        List<String> values = [
          data['uuid'],
          data['nip'],
          data['nama'],
          data['id_karyawan'].toString(),
          data['role'],
          data['roleId'].toString(),
          data['email'],
          data['kantor']
        ];

        Toast.show(post['msg'], context, duration: 3, gravity: Toast.CENTER);

        await Storage.setStorage(keys, values);

        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return Home();
        }));
      } else {
        setState(() {
          this.textButton = "Masuk";
        });
        Toast.show(post.toString(), context,
            duration: 3, gravity: Toast.CENTER);
      }
    } catch (e) {
      setState(() {
        this.textButton = "Masuk";
      });
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 130, 10, 0),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.white60,
                child: Image.asset(
                  "assets/launcher/phc.png",
                  width: 70,
                ),
                radius: 50,
              ),
              SizedBox(
                height: 20,
              ),
              Card(
                elevation: 3,
                child: Padding(
                  padding: EdgeInsets.all(30),
                  child: Column(
                    children: [
                      Fonts.headings("Login GPSM", true, Colors.blueAccent),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: "Masukkan NIP"),
                        onChanged: (value) {
                          setState(() {
                            _username = value;
                          });
                        },
                      ),
                      TextFormField(
                        obscureText: show,
                        decoration: InputDecoration(
                            labelText: "Masukkan Password",
                            suffixIcon: IconButton(
                                icon: Icon(suffix),
                                onPressed: () {
                                  setState(() {
                                    if (show) {
                                      show = false;
                                      suffix = Icons.lightbulb;
                                    } else {
                                      show = true;
                                      suffix = Icons.lightbulb_outline;
                                    }
                                  });
                                })),
                        onChanged: (value) {
                          setState(() {
                            _password = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      FlatButton(
                          minWidth: 400,
                          height: 40,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          color: Colors.blueAccent,
                          onPressed: () {
                            setState(() {
                              this.textButton = "Loading ...";
                            });
                            if (_username == null || _username == "") {
                              Toast.show("Masukkan NIP Anda", context,
                                  duration: 3, gravity: Toast.CENTER);
                              setState(() {
                                this.textButton = "Masuk";
                              });
                            } else if (_password == null || _password == "") {
                              Toast.show("Masukkan Password Anda", context,
                                  duration: 3, gravity: Toast.CENTER);
                              setState(() {
                                this.textButton = "Masuk";
                              });
                            } else {
                              var object = {
                                'username': _username,
                                'password': _password
                              };
                              this.postData(object);
                            }
                          },
                          child: Fonts.subHeadings(
                              textButton, false, false, Colors.white))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
