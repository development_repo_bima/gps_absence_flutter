import 'package:tps/activity/absensi_karyawan.dart';
import 'package:tps/component/components.dart';
import 'package:tps/service/route.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Kehadiran extends StatefulWidget {
  Kehadiran({Key key}) : super(key: key);

  @override
  _KehadiranState createState() => _KehadiranState();
}

class _KehadiranState extends State<Kehadiran> {
  final String url =
      'https://dev-admin.bimsalabimm.space/frontend/web/api/update-kehadiran';
  List data;
  Widget body;

  Widget loading() {
    return CircularProgressIndicator();
  }

  Widget getResult() {
    return ListView.builder(
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            print(data[index]["iId"]);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Absensi(
                        from: "Update",
                        id: data[index]["iId"].toString(),
                        nama: data[index]["vKeterangan"])));
          },
          child: Card(
            elevation: 3,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(data[index]["vKeterangan"]),
            ),
          ),
        );
      },
      itemCount: data.length,
    );
  }

  Widget getEmpty() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Card(
            elevation: 3,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text("Data Tidak ditemukan"),
              ),
            ))
      ],
    );
  }

  Future<String> getData() async {
    // MEMINTA DATA KE SERVER DENGAN KETENTUAN YANG DI ACCEPT ADALAH JSON
    var res = await http.get(Uri.encodeFull(Routing.update),
        headers: {'accept': 'application/json'});

    try {
      var body = json.decode(res.body);

      if (body['status']) {
        setState(() {
          data = body['data'];
          // _showMyDialog(body['msg']);
          // this.body =
          // Components.resultData(data, Absensi(), ['iId', 'vKeterangan']);
          this.body = this.getResult();
        });
      } else {
        this.body = getEmpty();
        _showMyDialog(body['msg']);
      }
      print(body);
      print(data);
    } catch (e) {
      this.body = getEmpty();
      print(e);
    }

    // return 'success!';
  }

  Future<void> _showMyDialog(msg) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Informasi'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(msg),
                // Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    // var clas = new Components();
    this.body = Components.loadingProcess("Loading ...");
    this.getData(); //PANGGIL FUNGSI YANG TELAH DIBUAT SEBELUMNYA
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [],
          title: Text("Update Kehadiran"),
        ),
        body: this.body);
  }
}
