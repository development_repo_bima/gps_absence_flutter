import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class MarketingReport extends StatefulWidget {
  MarketingReport({Key key}) : super(key: key);

  @override
  _MarketingReportState createState() => _MarketingReportState();
}

class _MarketingReportState extends State<MarketingReport> {
  String _user = "-";
  Map data;
  List result;
  bool _isLoading = true;
  String _bulandipilih, _tahundipilih;
  List _bulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];
  List _tahun = ["2019", "2020", "2021"];

  Future init() async {
    List<String> keys = [
      'uuid',
      'nip',
      'nama',
    ];
    Map storage = await Storage.getStorage(keys);
    setState(() {
      this.data = storage;
      this._user = storage['nama'] + ' - ' + storage['nip'];
      this._isLoading = false;
    });
    // Toast.show(storage.toString(), context, duration: 3, gravity: Toast.CENTER);
  }

  Future<Map> getData() async {
    try {
      // var offset = page - 1;
      var req = new Request();

      var res = await req.getRequest(Routing.absensi +
          '?month=$_bulandipilih&year=$_tahundipilih&id_karyawan=');

      if (res['status']) {
        if (this.mounted) {
          setState(() {
            result = res['data'];
            _isLoading = false;
          });
        }
        // Toast.show(result.toString(), context,
        //     duration: 3, gravity: Toast.CENTER);
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  void initState() {
    this.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Fonts.headings("Report Absensi", false, Colors.white),
      ),
      body: Container(
        child: (this._isLoading)
            ? Components.loadingProcess("Loading ...")
            : Card(
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(7),
                  child: Stack(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(4),
                          child: Column(
                            children: [
                              TextFormField(
                                initialValue: _user,
                                decoration: InputDecoration(labelText: 'User'),
                                validator: (value) {
                                  // if (value.isEmpty) {
                                  //   return 'Masukkan NIP Anda.';
                                  // }
                                },
                                // onSaved: (val) =>
                                //     setState(() => _keterangan = val),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  DropdownButton(
                                    hint: Text("Pilih Bulan"),
                                    value: _bulandipilih,
                                    items: _bulan.map((value) {
                                      return DropdownMenuItem(
                                        child: Text(value),
                                        value: value,
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _bulandipilih =
                                            value; //Untuk memberitahu _valFriends bahwa isi nya akan diubah sesuai dengan value yang kita pilih
                                      });
                                    },
                                  ),
                                  DropdownButton(
                                    hint: Text("Pilih Tahun"),
                                    value: _tahundipilih,
                                    items: _tahun.map((value) {
                                      return DropdownMenuItem(
                                        child: Text(value),
                                        value: value,
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _tahundipilih =
                                            value; //Untuk memberitahu _valFriends bahwa isi nya akan diubah sesuai dengan value yang kita pilih
                                      });
                                    },
                                  ),
                                  RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18)),
                                    onPressed: () {
                                      if (_bulandipilih == null) {
                                        Toast.show("Pilih Bulan Absen", context,
                                            duration: 3, gravity: Toast.CENTER);
                                      } else if (_tahundipilih == null) {
                                        Toast.show("Pilih Tahun Absen", context,
                                            duration: 3, gravity: Toast.CENTER);
                                      } else {
                                        this.getData();
                                      }
                                    },
                                    color: Colors.green,
                                    child: Icon(
                                      Icons.search,
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              (result == null)
                                  ? Container()
                                  : Container(
                                      height: 400,
                                      child: SingleChildScrollView(
                                        child: DataTable(
                                            columns: <DataColumn>[
                                              DataColumn(
                                                  label: Fonts.subHeadings(
                                                      "Tanggal",
                                                      true,
                                                      false,
                                                      Colors.black)),
                                              DataColumn(
                                                  label: Fonts.subHeadings(
                                                      "Masuk",
                                                      true,
                                                      false,
                                                      Colors.black)),
                                              DataColumn(
                                                  label: Fonts.subHeadings(
                                                      "Keluar",
                                                      true,
                                                      false,
                                                      Colors.black)),
                                            ],
                                            rows: result
                                                .map((e) => DataRow(cells: [
                                                      DataCell(Fonts.paragraph(
                                                          e['date'],
                                                          false,
                                                          false,
                                                          Colors.black)),
                                                      DataCell(Fonts.paragraph(
                                                          e['ci'],
                                                          false,
                                                          false,
                                                          Colors.black)),
                                                      DataCell(Fonts.paragraph(
                                                          e['co'],
                                                          false,
                                                          false,
                                                          Colors.black)),
                                                    ]))
                                                .toList()),
                                      ),
                                    )
                            ],
                          ))
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
