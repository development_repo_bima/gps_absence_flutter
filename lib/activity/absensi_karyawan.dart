import 'package:tps/activity/home.dart';
import 'package:tps/activity/login.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:trust_location/trust_location.dart';
import 'package:toast/toast.dart';

class Absensi extends StatefulWidget {
  final String from;
  final String id;
  final String nama;
  Absensi({Key key, this.from, this.id, this.nama}) : super(key: key);

  @override
  _AbsensiState createState() => _AbsensiState();
}

class _AbsensiState extends State<Absensi> {
  String _keterangan, _tagLokasi, _text;
  String _choosingLocation = "-";
  Position _currentPosition;

  List _absensi = ["Absen Masuk", "Absen Keluar"];
  List<Address> results = [];
  List _locationString = ["-"];
  bool _isLoading = true;

  bool mockStatus = false;
  bool loadLocation = false;
  Widget checkForm;
  String _postButton = "Post";

  String _latitude;
  String _longitude;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  /// get location method, use a try/catch PlatformException.
  Future<void> getLocation() async {
    try {
      TrustLocation.onChange.listen((values) => setState(() {
            _latitude = values.latitude;
            _longitude = values.longitude;
            mockStatus = values.isMockLocation;
          }));
    } on PlatformException catch (e) {
      print('PlatformException $e');
    }
  }

  _getCurrentLocation() {
    setState(() {
      loadLocation = true;
    });
    // requestLocationPermission();

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        // _params.add({'latitude': _currentPosition.latitude},
        //     {'longitude': _currentPosition.longitude});
      });

      _getAddressFromLatLng();
      TrustLocation.start(3);
      setState(() {
        if (this.mockStatus == true) {
          this._showMyDialog(context);
        }
      });
      setState(() {
        _isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      final coordinates =
          Coordinates(_currentPosition.latitude, _currentPosition.longitude);

      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);

      for (var i = 0; i < addresses.length; i++) {
        String line = "${addresses[i].addressLine}";
        // print(line);
        setState(() {
          _locationString.add(line);
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
    getLocation();
  }

  Widget check() {
    return DropdownButton(
      hint: Text("Pilih Jenis Absensi"),
      value: _keterangan,
      items: _absensi.map((value) {
        return DropdownMenuItem(
          child: Text(value),
          value: value,
        );
      }).toList(),
      onChanged: (value) {
        setState(() {
          _keterangan = value;
          //form_keterangan.text = value;
        });
      },
    );
  }

  Future postData(json) async {
    try {
      var req = new Request();
      // Toast.show(json.toString(), context, duration: 3, gravity: Toast.CENTER);
      Map<String, dynamic> post =
          await req.postRequest(Routing.post, body: json);

      if (!post['auth_validation']) {
        Toast.show("Sesi Login anda sudah habis, silahkan login ulang", context,
            duration: 2, gravity: Toast.CENTER);

        // Navigator.pushReplacement(context,
        //     MaterialPageRoute(builder: (BuildContext context) {
        //   return Login();
        // }));
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => Login()),
          (Route<dynamic> route) => false,
        );
      } else if (post['status']) {
        Toast.show(post['msg'], context, duration: 2, gravity: Toast.CENTER);

        // Navigator.pushReplacement(context,
        //     MaterialPageRoute(builder: (BuildContext context) {
        //   return Home();
        // }));
        Future.delayed(const Duration(seconds: 2), () {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => Home()),
            (Route<dynamic> route) => false,
          );
        });
      } else {
        Toast.show(post['msg'], context, duration: 2, gravity: Toast.CENTER);
      }
      setState(() {
        this._postButton = "Post";
      });
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
      setState(() {
        this._postButton = "Post";
      });
    }
  }

  Future<bool> _showMyDialog(context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Perhatian'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    "Anda Terdeteksi Menggunakan Lokasi Palsu, silahkan matikan lokasi palsu dan uninstall aplikasi lokasi palsu"),
                // Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            // TextButton(
            //   child: Text('Batal'),
            //   onPressed: () {
            //     Navigator.of(context).pop();
            //   },
            // ),
            TextButton(
              child: Text('Mengerti'),
              onPressed: () async {
                try {
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                } catch (e) {
                  Toast.show(e.toString(), context,
                      duration: 2, gravity: Toast.CENTER);
                }
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: (widget.from == "Update")
              ? Fonts.headings(widget.nama, false, Colors.white)
              : Fonts.headings("Absensi", false, Colors.white),
          actions: [],
        ),
        body: (_isLoading)
            ? Components.loadingProcess("Mendapatkan Lokasi Anda ...")
            : Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                // height: 220,
                width: double.infinity,
                // width: double.maxFinite,
                child: SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    (widget.from == "Update")
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.all(4),
                            child: Center(
                              child: Fonts.subHeadings("Absensi Masuk / Keluar",
                                  true, false, Colors.black),
                            ),
                          ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      elevation: 5,
                      child: Padding(
                          padding: EdgeInsets.all(7),
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                // controller: form_keterangan,
                                // initialValue: _keterangan,
                                decoration: InputDecoration(
                                    labelText: 'Tuliskan Alasan Anda'),

                                onChanged: (val) {
                                  setState(() {
                                    _text = val;
                                  });
                                },
                              ),
                              TextFormField(
                                // controller: form_keterangan,
                                // initialValue: _keterangan,
                                decoration: InputDecoration(
                                    labelText: 'Keterangan Tempat'),

                                onChanged: (val) {
                                  setState(() {
                                    _tagLokasi = val;
                                  });
                                },
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              DropdownButton<String>(
                                isExpanded: true,
                                hint: Text("Pilih Lokasi"),
                                value: _choosingLocation,
                                items: _locationString.map((value) {
                                  return DropdownMenuItem<String>(
                                    child: Fonts.paragraph(
                                        value, false, false, Colors.black),
                                    value: value,
                                  );
                                }).toList(),
                                onChanged: (val) {
                                  setState(() {
                                    _choosingLocation =
                                        val; //Untuk memberitahu _valFriends bahwa isi nya akan diubah sesuai dengan value yang kita pilih
                                  });
                                  val = "pilih";
                                },
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Text(
                                _choosingLocation,
                                style: TextStyle(
                                    fontSize: 12, fontStyle: FontStyle.italic),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              (widget.from == "Update")
                                  ? Container()
                                  : DropdownButton(
                                      hint: Text("Pilih Jenis Absensi"),
                                      value: _keterangan,
                                      items: _absensi.map((value) {
                                        return DropdownMenuItem(
                                          child: Fonts.paragraph(value, false,
                                              false, Colors.black),
                                          value: value,
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        setState(() {
                                          _keterangan = value;
                                          //form_keterangan.text = value;
                                        });
                                      },
                                    ),
                              Container(
                                width: 400,
                                child: RaisedButton(
                                  onPressed: () async {
                                    setState(() {
                                      this._postButton = "Loading ...";
                                    });
                                    var post = false;
                                    var _par = {};
                                    if (_choosingLocation == '-') {
                                      Toast.show("Pilih Lokasi Anda", context,
                                          duration: 3, gravity: Toast.CENTER);
                                      post = false;
                                    } else if (widget.from != "Update" &&
                                        _keterangan == null) {
                                      Toast.show("Pilih jenis absensi", context,
                                          duration: 3, gravity: Toast.CENTER);
                                      post = false;
                                    } else if (widget.from == "Update" &&
                                        _text == null) {
                                      Toast.show(
                                          "Harap isi alasan anda", context,
                                          duration: 3, gravity: Toast.CENTER);
                                      post = false;
                                    } else {
                                      _par = {
                                        'longitude': _longitude,
                                        'latitude': _latitude,
                                        'address': _choosingLocation,
                                        'type': (_keterangan == null)
                                            ? "Update"
                                            : _keterangan,
                                        'id_update': (widget.id != null)
                                            ? widget.id
                                            : '0',
                                        'tag': _tagLokasi,
                                        'text': (_text == null) ? "-" : _text,
                                      };
                                      post = true;
                                    }

                                    if (post) {
                                      this.postData(_par);
                                    }
                                    setState(() {
                                      this._postButton = "Post";
                                    });
                                  },
                                  color: Colors.lightBlue,
                                  child: Fonts.subHeadings(this._postButton,
                                      false, false, Colors.white),
                                ),
                              )
                            ],
                          )),
                    ),
                  ],
                ))));
  }
}
