import 'dart:convert';
import 'dart:typed_data';

import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DetailKunjungan extends StatefulWidget {
  final String id;

  DetailKunjungan({Key key, this.id}) : super(key: key);
  @override
  _DetailKunjunganState createState() => _DetailKunjunganState();
}

class _DetailKunjunganState extends State<DetailKunjungan> {
  Map data;
  Map storage;
  var listProduk;
  bool _isLoading = true;
  Uint8List signPng;
  String signBlob;
  bool _isEmpty = false;

  Future<void> getDetail() async {
    try {
      var req = new Request();
      String uid = (widget.id.isEmpty) ? '0' : widget.id;
      var res = await req.getRequest(Routing.reportMarketing + '?id=$uid');
      // Toast.show(res.toString(), context, duration: 3, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              // Toast.show(res['data'].toString(), context,
              //     duration: 3, gravity: Toast.CENTER);
              data = res['data'];
              _isEmpty = false;
              if (data['vImgSign'] != null || data['vImgSign'] != "") {
                this.signPng = base64Decode(data['vImgSign']);
                this.signBlob = data['vImgSign'];
              }
              // String jsonProduk = data['vListProduk'].toString();
              data['vListProduk'] = data['vListProduk']
                  .replaceAll('[', '')
                  .replaceAll('{', '')
                  .replaceAll(']', '')
                  .replaceAll('}', '')
                  .replaceAll('id:', '');
            } else {
              _isEmpty = true;
            }
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      Toast.show(e.toString() + 'catch', context,
          duration: 3, gravity: Toast.CENTER);
    }
  }

  Future init() async {
    Map storage = await Storage.getStorage(['id_karyawan', 'role_id']);
    setState(() {
      this.storage = storage;
    });
    // Toast.show(this.storage['id_karyawan'], context,
    //     duration: 3, gravity: Toast.CENTER);
  }

  @override
  void initState() {
    super.initState();
    this.init();
    this.getDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Report Kunjungan"),
      ),
      body: (_isLoading)
          ? Components.loadingProcess("Loading ...")
          : Container(
              padding: EdgeInsets.only(top: 20),
              width: double.infinity,
              child: Container(
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    Card(
                      elevation: 3,
                      child: Padding(
                        padding: EdgeInsets.all(6),
                        child: Column(
                          children: [
                            Fonts.headings(
                                "Report Kunjungan", true, Colors.black),
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: DataTable(
                                  columns: <DataColumn>[
                                    DataColumn(label: Text("")),
                                    DataColumn(label: Text("")),
                                    DataColumn(label: Text("")),
                                  ],
                                  dataRowHeight: 65,
                                  columnSpacing: 5,
                                  rows: <DataRow>[
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph(
                                            "Nama Marketing",
                                            false,
                                            false,
                                            Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ((data['nama_marketing']) ?? '-'),
                                            // "Rumah Sakit Tes",
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Outlet",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ((data['nama_outlet']) ?? '-') +
                                                ' - ' +
                                                ((data['kode_outlet']) ?? '-'),
                                            // "Rumah Sakit Tes",
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph(
                                            "Jenis Kunjungan",
                                            false,
                                            false,
                                            Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['eJenis']) ?? '-',
                                            // "Direct Call",
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Ruangan",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['eRuangan']) ?? '-',
                                            // "ICU",
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Nama PIC",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['vNamaPic']) ?? '-',
                                            // "darto",
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Jabatan PIC",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            (data['vJabatanPic']) ?? '-',
                                            // "teknisi ICU",
                                            false,
                                            false,
                                            Colors.black)),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph("Komentar PIC",
                                            false, false, Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(SingleChildScrollView(
                                          child: Fonts.paragraph(
                                              (data['tKomentarPic']) ?? '-',
                                              // "Beberapa Produk memang diperlukan",
                                              false,
                                              false,
                                              Colors.black),
                                        )),
                                      ],
                                    ),
                                    DataRow(
                                      cells: <DataCell>[
                                        DataCell(Fonts.paragraph(
                                            "Keterangan SR",
                                            false,
                                            false,
                                            Colors.black)),
                                        DataCell(Fonts.paragraph(
                                            ":", false, false, Colors.black)),
                                        DataCell(SingleChildScrollView(
                                          child: Fonts.paragraph(
                                              (data['tKeteranganSr']) ?? '-',
                                              // "untuk produk pencil dapat masuk jika harga e-catalog",
                                              false,
                                              false,
                                              Colors.black),
                                        )),
                                      ],
                                    ),
                                  ]),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Fonts.paragraph(
                                "TTD PIC", false, false, Colors.black),
                            Container(
                              height: 170,
                              width: 250,
                              child: Container(
                                  width: 120,
                                  color: Colors.grey[300],
                                  child: Image.memory(this.signPng)),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Fonts.paragraph("Produk yg dibahas", false, false,
                                Colors.black),
                            Container(
                              height: 120,
                              child: Fonts.paragraph(
                                  (data['vListProduk']) ?? '-',
                                  // "untuk produk pencil dapat masuk jika harga e-catalog",
                                  false,
                                  false,
                                  Colors.black),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                )),
              ),
            ),
    );
  }
}
