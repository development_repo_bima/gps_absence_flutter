import 'package:tps/component/fonts.dart';
import 'package:flutter/material.dart';

class ListKunjungan extends StatefulWidget {
  ListKunjungan({Key key}) : super(key: key);

  @override
  _ListKunjunganState createState() => _ListKunjunganState();
}

class _ListKunjunganState extends State<ListKunjungan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Fonts.headings("List Kunjungan", false, Colors.white),
      ),
    );
  }
}
