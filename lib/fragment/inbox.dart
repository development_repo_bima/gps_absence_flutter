import 'package:tps/activity/detail.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/request.dart';
import 'package:tps/service/route.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class Inbox extends StatefulWidget {
  Inbox({Key key}) : super(key: key);

  @override
  _InboxState createState() => _InboxState();
}

class _InboxState extends State<Inbox> {
  ScrollController _scrollController = new ScrollController();
  List data;
  Map safe;
  bool _isLoading = true;
  bool _isEmpty = true;
  bool _isMore = false;
  bool _isScrollable = true;
  int page = 1;
  int limit = 10;
  String _lokasi = "";
  String url;

  @override
  void initState() {
    super.initState();
    // var clas = new Components();
    this.init();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          this.page = this.page + 1;
        });
        if (this._isScrollable) {
          getMore(this.page);
        }
      }
    });
  }

  Future init() async {
    List<String> keys = [
      'nama',
      'role',
      'role_id',
    ];
    Map storage = await Storage.getStorage(keys);
    setState(() {
      this.safe = storage;
      this._lokasi = (storage['kantor'] == "PHC")
          ? "PT Prima Health Care"
          : "PT Total Medika Persada";
      // this._isLoading = false;
      // this.url =
      //     (storage['role_id'] == "6") ? Routing.inbox_marketing : Routing.inbox;

      if (storage['role_id'] == '5' ||
          storage['role_id'] == '6' ||
          storage['role_id'] == '13' ||
          storage['role_id'] == '15' ||
          storage['role_id'] == '19') {
        this.url = Routing.inbox_marketing;
      } else if (storage['role_id'] == '22') {
        this.url = Routing.inboxManager;
      } else {
        this.url = Routing.inbox;
      }
    });
    this.getData();
    // Toast.show(storage.toString(), context, duration: 3, gravity: Toast.CENTER);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<Map> getData() async {
    try {
      var offset = page - 1;
      var req = new Request();

      var res = await req.getRequest(url + '?offset=$offset&limit=$limit&q=');
      // Toast.show(res.toString(), context, duration: 3, gravity: Toast.CENTER);
      if (res['status']) {
        if (this.mounted) {
          setState(() {
            if (res['data'].isNotEmpty) {
              data = res['data'];
              _isEmpty = false;
            } else {
              _isEmpty = true;
            }
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      Toast.show(e.toString(), context, duration: 3, gravity: Toast.CENTER);
    }
  }

  void getMore(page) async {
    if (!_isMore) {
      setState(() {
        this._isMore = true;
      });
    }

    if (this._isMore) {
      this._showMyDialog(context, "Loading");
    }

    int nextpage = page - 1;
    int offset = (limit * nextpage) + 1;

    var req = new Request();
    var res = await req.getRequest(url + '?offset=$offset&limit=$limit&q=');

    if (res['data'].isNotEmpty) {
      if (this.mounted) {
        setState(() {
          data.addAll(res['data']);
          _isMore = false;
          Navigator.of(context).pop();
          _isScrollable = true;
        });
      }
    } else {
      Toast.show("Telah mencapai baris terakhir, tidak bisa melanjutkan scroll",
          context,
          duration: 1, gravity: Toast.CENTER);
      setState(() {
        _isScrollable = false;
      });
      Navigator.of(context).pop();
    }
  }

  Future<bool> _showMyDialog(context, msg) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          // title: Text('Informasi'),
          content: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(6),
              child: Row(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    width: 8,
                  ),
                  Fonts.paragraph("Loading ...", false, false, Colors.black)
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return (_isLoading)
        ? Components.loadingProcess("Loading ...")
        : (_isEmpty)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Card(
                      elevation: 0,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Fonts.headings(
                                  "Data Kosong", false, Colors.black),
                            ],
                          ),
                        ),
                      ))
                ],
              )
            : Container(
                child: ListView.builder(
                  controller: _scrollController,
                  itemBuilder: (context, index) {
                    return Card(
                        elevation: 2,
                        borderOnForeground: true,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Detail(
                                          id: data[index]['id'],
                                          flag: data[index]['flag'],
                                        )));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.done_all,
                                  size: 25,
                                  color: (data[index]['flag'] == "1")
                                      ? Colors.green
                                      : Colors.red,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Fonts.paragraph(
                                        data[index]['post_date'].toString(),
                                        false,
                                        true,
                                        Colors.blue),
                                    Fonts.subHeadings(
                                        (data[index]['nama_karyawan'] ?? '-') +
                                            ' (' +
                                            (data[index]['jabatan'] ?? '-') +
                                            ')',
                                        false,
                                        true,
                                        Colors.blueGrey),
                                    Fonts.subHeadings(
                                        data[index]['tipe'] +
                                            " - " +
                                            data[index]['lokasi'],
                                        false,
                                        false,
                                        Colors.black),
                                    Fonts.paragraph(data[index]['text'], false,
                                        false, Colors.black),
                                  ],
                                ))
                              ],
                            ),
                          ),
                        ));
                  },
                  itemCount: data.length,
                ),
              );
  }
}
