import 'package:tps/activity/login.dart';
import 'package:tps/component/components.dart';
import 'package:tps/component/fonts.dart';
import 'package:tps/service/storage.dart';
import 'package:flutter/material.dart';
// import 'package:crypto_font_icons/crypto_font_icons.dart';
// import 'package:typicons_flutter/typicons.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class Account extends StatefulWidget {
  Account({Key key}) : super(key: key);

  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  Map data;
  String _lokasi = "-";
  bool _isLoading = true;
  @override
  Widget build(BuildContext context) {
    return (_isLoading)
        ? Components.loadingProcess("Loading ...")
        : Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [Colors.blueGrey, Colors.lightBlueAccent])),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Card(
                          elevation: 2,
                          child: Padding(
                            padding: EdgeInsets.all(12),
                            child: Row(
                              children: [
                                CircleAvatar(
                                  child: Icon(Icons.data_usage_sharp),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Fonts.subHeadings(
                                              this.data['nama'] ?? '-',
                                              true,
                                              false,
                                              Colors.black),
                                        ),
                                        Fonts.paragraph(
                                            ' ( ' +
                                                (this.data['nip'] ?? '-') +
                                                ' )',
                                            true,
                                            true,
                                            Colors.grey),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 6,
                                    ),
                                    // Fonts.paragraph(this.data['email'] ?? '-',
                                    //     true, true, Colors.grey),
                                    Fonts.paragraph(this.data['role'] ?? '-',
                                        true, false, Colors.black),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                            elevation: 4,
                            child: Padding(
                              padding: EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  Icon(Icons.arrow_forward_ios),
                                  Fonts.paragraph(
                                      "Nama : ", false, false, Colors.black),
                                  Fonts.paragraph(this.data['nama'] ?? '-',
                                      false, false, Colors.black),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                            elevation: 4,
                            child: Padding(
                              padding: EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  Icon(Icons.arrow_forward_ios),
                                  Fonts.paragraph(
                                      "NIP     : ", false, false, Colors.black),
                                  Fonts.paragraph(this.data['nip'] ?? '-',
                                      false, false, Colors.black),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                            elevation: 4,
                            child: Padding(
                              padding: EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  Icon(Icons.arrow_forward_ios),
                                  Fonts.paragraph(
                                      "Email  : ", false, false, Colors.black),
                                  Fonts.paragraph(this.data['email'] ?? '-',
                                      false, false, Colors.black),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                            elevation: 4,
                            child: Padding(
                              padding: EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  Icon(Icons.arrow_forward_ios),
                                  Fonts.paragraph("Lokasi Kerja : ", false,
                                      false, Colors.black),
                                  Fonts.paragraph(this._lokasi ?? '-', false,
                                      false, Colors.black),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                              elevation: 4,
                              child: InkWell(
                                onTap: () {
                                  this._showMyDialog(context);
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Row(
                                    children: [
                                      Icon(Icons.logout),
                                      Fonts.paragraph(
                                          "Keluar", true, false, Colors.red)
                                    ],
                                  ),
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 30,
                        )
                      ],
                    ),
                  )),
            ],
          );
  }

  Future<bool> _showMyDialog(context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Konfirmasi'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Keluar dari Aplikasi ?"),
                // Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Batal'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Ya'),
              onPressed: () async {
                try {
                  Storage.clearStorage();
                  var check = await Storage.containStorage('uuid');
                  if (check) {
                    Toast.show(
                        "Terjadi Kesalahan, coba beberapa saat lagi", context,
                        duration: 2, gravity: Toast.CENTER);
                  } else {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Login();
                    }));
                  }
                } catch (e) {
                  Toast.show(e.toString(), context,
                      duration: 2, gravity: Toast.CENTER);
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future init() async {
    List<String> keys = [
      'uuid',
      'nip',
      'nama',
      'role',
      'role_id',
      'email',
      'kantor'
    ];
    Map storage = await Storage.getStorage(keys);

    setState(() {
      this.data = storage;
      this._lokasi = (storage['kantor'] == "PHC")
          ? "PT Prima Health Care"
          : "PT Total Medika Persada";
      this._isLoading = false;
    });
    // Toast.show(storage.toString(), context, duration: 3, gravity: Toast.CENTER);
  }

  void initState() {
    this.init();
    super.initState();
  }
}
