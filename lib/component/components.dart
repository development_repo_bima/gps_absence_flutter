import 'package:tps/component/fonts.dart';
import 'package:flutter/material.dart';

class Components {
  static Widget emptyData() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Card(
            elevation: 3,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child:
                    Fonts.headings("Data Tidak Ditemukan", false, Colors.black),
              ),
            ))
      ],
    );
  }

  // static Widget resultData(List data, route, column) {
  //   return ListView.builder(
  //     itemBuilder: (context, index) {
  //       return GestureDetector(
  //         onTap: () {
  //           // print(data[index]["iId"]);
  //           Navigator.push(
  //               context,
  //               MaterialPageRoute(
  //                   builder: (context) => route(
  //                         from: "Update",
  //                         id: data[index][column[0]],
  //                       )));
  //         },
  //         child: Card(
  //           elevation: 3,
  //           child: Padding(
  //             padding: const EdgeInsets.all(10.0),
  //             child: Fonts.paragraph(
  //                   data[index][column[1]], false, false, Colors.black),
  //             ),
  //           ),
  //         ),
  //       )
  //     },
  //     itemCount: data.length,
  //   );
  // }

  static Widget loadingProcess(msg) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Card(
            elevation: 0,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      width: 10,
                    ),
                    Fonts.headings(msg, false, Colors.black),
                  ],
                ),
              ),
            ))
      ],
    );
  }

  static Widget check(String _keterangan, List _absensi) {
    return DropdownButton(
      hint: Text("Pilih Jenis Absensi"),
      value: _keterangan,
      items: _absensi.map((value) {
        return DropdownMenuItem(
          child: Text(value),
          value: value,
        );
      }).toList(),
      onChanged: (value) {
        // setState(() {
        //   _keterangan = value;
        //   //form_keterangan.text = value;
        // });
      },
    );
  }

  static Future<bool> _showMyDialog(context, msg, confirm) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Informasi'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(msg),
                // Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            (confirm)
                ? TextButton(
                    child: Text('Batal'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      return false;
                    },
                  )
                : Container(),
            TextButton(
              child: (confirm) ? Text('Ya') : Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
                return true;
              },
            ),
          ],
        );
      },
    );
  }
}
