import 'package:flutter/material.dart';

class Fonts {
  static Widget headings(msg, bold, color) {
    return Text(
      msg,
      style: TextStyle(
          fontSize: 16,
          fontWeight: (bold) ? FontWeight.bold : FontWeight.normal,
          color: color),
    );
  }

  static Widget subHeadings(msg, bold, italic, color) {
    return Text(
      msg,
      style: TextStyle(
          fontSize: 14,
          fontWeight: (bold) ? FontWeight.bold : FontWeight.normal,
          fontStyle: (italic) ? FontStyle.italic : FontStyle.normal,
          color: color),
    );
  }

  static Widget paragraph(msg, bold, italic, color) {
    return Text(
      msg,
      style: TextStyle(
          fontSize: 12,
          fontWeight: (bold) ? FontWeight.bold : FontWeight.normal,
          fontStyle: (italic) ? FontStyle.italic : FontStyle.normal,
          color: color),
    );
  }

  static Widget subParagraph(msg, bold, italic, color) {
    return Text(
      msg,
      style: TextStyle(
          fontSize: 10,
          fontWeight: (bold) ? FontWeight.bold : FontWeight.normal,
          fontStyle: (italic) ? FontStyle.italic : FontStyle.normal,
          color: color),
    );
  }
}
